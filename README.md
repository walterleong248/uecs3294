# UECS3294 Advanced Web Application Development #


### What is this repository for? ###

This repository exists for the purpose of the assignment submission.

### Contents ###

- This repository contains a .zip file for the assignment submission.
- The SQL script for the *database config* & *the project folder* has been attached to the .zip file.


### Who do I talk to? ###

If any issues persisting with the folder, please contact me @ ***leonghoiweng@hotmail.com*** or ***walterleong.95@1utar.my***.
* Other community or team contact